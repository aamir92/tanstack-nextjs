"use client";
import React, { FC, useState } from "react";
import { DndProvider, useDrag, useDrop } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import CustomSelect from "@components/custom-select";
import { FaEdit, FaTrash, FaArrowDown, FaArrowUp } from "react-icons/fa";
import {
  MdKeyboardArrowLeft,
  MdKeyboardDoubleArrowLeft,
  MdKeyboardDoubleArrowRight,
  MdKeyboardArrowRight,
} from "react-icons/md";

import {
  Column,
  ColumnDef,
  ColumnOrderState,
  flexRender,
  getCoreRowModel,
  Header,
  Table,
  useReactTable,
  ColumnResizeMode,
  getFilteredRowModel,
  getPaginationRowModel,
  createTable,
  getSortedRowModel,
  SortingState,
} from "@tanstack/react-table";

type Person = {
  Name: string;
  Priority: string;
  Phone: string;
  Email: string;
  Address: string;
  Notes: string;
};

interface TableTSProps {
  data: Person[];
}

function reorderColumn(
  draggedColumnId: string,
  targetColumnId: string,
  columnOrder: string[],
): ColumnOrderState {
  columnOrder.splice(
    columnOrder.indexOf(targetColumnId),
    0,
    columnOrder.splice(columnOrder.indexOf(draggedColumnId), 1)[0] as string,
  );
  return [...columnOrder];
}

const defaultColumns: ColumnDef<Person>[] = [
  {
    accessorKey: "Name",
    id: "name",
    header: "Name",
    cell: (info) => info.getValue(),
  },
  {
    accessorFn: (row) => row.Priority,
    id: "priority",
    cell: (info) => info.getValue(),
    header: () => <span>Priority</span>,
  },
  {
    accessorKey: "Phone",
    id: "phone",
    header: "Phone",
  },
  {
    accessorKey: "Email",
    id: "email",
    header: "Email",
  },
  {
    accessorKey: "Address",
    id: "address",
    header: "Address",
  },
  {
    accessorKey: "Notes",
    id: "notes",
    header: "Notes",
  },
  {
    accessorKey: "Actions",
    id: "actions",
    header: "Actions",
  },
];

const TableTS: FC<TableTSProps> = ({ data }) => {
  const [columns] = useState(() => [...defaultColumns]);
  const options = ["High", "Low", "Medium"];
  const [sorting, setSorting] = React.useState<SortingState>([]);

  const [columnResizeMode, setColumnResizeMode] =
    useState<ColumnResizeMode>("onChange");

  const [columnOrder, setColumnOrder] = useState<ColumnOrderState>(
    columns.map((column) => column.id as string),
  );

  const updatePriority = (rowIndex: number, newPriority: string) => {
    const updatedData = [...data];
    updatedData[rowIndex].Priority = newPriority;
  };
  const handleOptionChange = (value: string, rowIndex: number) => {
    updatePriority(rowIndex, value);
  };

  const resetOrder = () =>
    setColumnOrder(columns.map((column) => column.id as string));

  const table = useReactTable({
    data,
    columns,
    state: {
      columnOrder,
      sorting,
    },
    onSortingChange: setSorting,
    onColumnOrderChange: setColumnOrder,
    getCoreRowModel: getCoreRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    debugTable: true,
    debugHeaders: true,
    debugColumns: true,
  });

  const TableHeaderCell: FC<{
    header: Header<Person, unknown>;
    table: Table<Person>;
  }> = ({ header, table }) => {
    const { getState, setColumnOrder } = table;
    const { columnOrder } = getState();
    const { column } = header;

    const [, dropRef] = useDrop({
      accept: "column",
      drop: (draggedColumn: Column<Person>) => {
        const newColumnOrder = reorderColumn(
          draggedColumn.id,
          column.id,
          columnOrder,
        );
        setColumnOrder(newColumnOrder);
      },
    });

    const [{ isDragging }, dragRef, previewRef] = useDrag({
      collect: (monitor) => ({
        isDragging: monitor.isDragging(),
      }),
      item: () => column,
      type: "column",
    });

    return (
      <th
        ref={dropRef}
        className="ui-table-cell"
        {...{
          key: header.id,
          colSpan: header.colSpan,
          style: { opacity: isDragging ? 0.5 : 1, width: header.getSize() },
        }}
      >
        <div ref={previewRef} className="flex items-center justify-center">
          {header.isPlaceholder ? null : (
            <div
              ref={dragRef}
              {...{
                className: header.column.getCanSort()
                  ? "cursor-pointer select-none"
                  : "",
                onClick: header.column.getToggleSortingHandler(),
              }}
            >
              {flexRender(header.column.columnDef.header, header.getContext())}
            </div>
          )}

          {{
            asc: <FaArrowUp />,
            desc: <FaArrowDown />,
          }[header.column.getIsSorted() as string] ?? null}

          {/* <button className="ml-2" ref={dragRef}>
            <FaArrowDown />
          </button> */}
        </div>
        <div
          {...{
            onMouseDown: header.getResizeHandler(),
            onTouchStart: header.getResizeHandler(),
            className: `resizer ${
              header.column.getIsResizing() ? "isResizing" : ""
            }`,
            style: {
              transform:
                columnResizeMode === "onChange" && header.column.getIsResizing()
                  ? `translateX(${
                      table.getState().columnSizingInfo.deltaOffset
                    }px)`
                  : "",
            },
          }}
        />
      </th>
    );
  };

  return (
    <div className="p-2">
      <DndProvider backend={HTML5Backend}>
        <div className="h-4" />
        <div className="flex flex-wrap gap-2">
          <button
            onClick={() => resetOrder()}
            className="w-32 rounded-sm border border-black border-opacity-20 bg-white px-5 py-2 text-sm"
          >
            Reset Order
          </button>
        </div>
        <div className="h-4"></div>
        <div className="overflow-x-auto bg-white">
          <table className="ui-table">
            <thead className="">
              {table.getHeaderGroups().map((headerGroup) => (
                <tr key={headerGroup.id} className="ui-table-row">
                  {headerGroup.headers.map((header) => (
                    <TableHeaderCell
                      key={header.id}
                      header={header}
                      table={table}
                    />
                  ))}
                </tr>
              ))}
            </thead>

            <tbody className="ui-table-body">
              {table.getRowModel().rows.map((row, rowIndex) => (
                <tr key={row.id} className="ui-table-row">
                  {row.getVisibleCells().map((cell) => (
                    <td
                      key={cell.id}
                      style={{
                        width: cell.column.getSize(),
                      }}
                      className={`ui-table-body-cell ${
                        cell.column.id === "priority"
                          ? row.original.Priority === "High"
                            ? "bg-red-500 text-white"
                            : row.original.Priority === "Medium"
                            ? "bg-yellow-500 text-white"
                            : "bg-green-500 text-white"
                          : cell.column.id === "phone"
                          ? " text-blue-500"
                          : ""
                      }`}
                    >
                      {cell.column.id === "priority" ? (
                        <CustomSelect
                          options={options}
                          defaultValue={row.original.Priority}
                          onChange={(value) =>
                            handleOptionChange(value, rowIndex)
                          }
                        />
                      ) : cell.column.id === "actions" ? (
                        <div className="flex items-center justify-center gap-10">
                          <FaTrash className="text-red-500 hover:cursor-pointer" />
                          <FaEdit className="text-primary hover:cursor-pointer" />
                        </div>
                      ) : (
                        flexRender(
                          cell.column.columnDef.cell,
                          cell.getContext(),
                        )
                      )}
                    </td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>

          <div className="p-2">
            <div className="flex items-center justify-between">
              <div className="flex gap-2">
                <select
                  className="h-6 rounded px-2"
                  value={table.getState().pagination.pageSize}
                  onChange={(e) => {
                    table.setPageSize(Number(e.target.value));
                  }}
                >
                  {[10, 20, 30, 40, 50].map((pageSize) => (
                    <option key={pageSize} value={pageSize}>
                      Show {pageSize}
                    </option>
                  ))}
                </select>
                <span className="flex items-center  gap-1">
                  <p className="mb-1">Go to page:</p>
                  <input
                    type="number"
                    defaultValue={table.getState().pagination.pageIndex + 1}
                    onChange={(e) => {
                      const page = e.target.value
                        ? Number(e.target.value) - 1
                        : 0;
                      table.setPageIndex(page);
                    }}
                    className="w-10 rounded border px-1 [appearance:textfield] [&::-webkit-inner-spin-button]:appearance-none [&::-webkit-outer-spin-button]:appearance-none"
                  />
                </span>
              </div>

              <div className="flex items-center space-x-2">
                <button
                  className="rounded border p-1 hover:bg-primary hover:text-white"
                  onClick={() => table.setPageIndex(0)}
                  disabled={!table.getCanPreviousPage()}
                >
                  <MdKeyboardArrowLeft />
                </button>
                <button
                  className="rounded border p-1 hover:bg-primary hover:text-white"
                  onClick={() => table.previousPage()}
                  disabled={!table.getCanPreviousPage()}
                >
                  <MdKeyboardDoubleArrowLeft />
                </button>
                <span className="flex items-center gap-1">
                  <div>Page</div>
                  <strong>
                    {table.getState().pagination.pageIndex + 1} of{" "}
                    {table.getPageCount()}
                  </strong>
                </span>
                <button
                  className="rounded border p-1 hover:bg-primary hover:text-white"
                  onClick={() => table.nextPage()}
                  disabled={!table.getCanNextPage()}
                >
                  <MdKeyboardDoubleArrowRight />
                </button>
                <button
                  className="rounded border p-1 hover:bg-primary hover:text-white"
                  onClick={() => table.setPageIndex(table.getPageCount() - 1)}
                  disabled={!table.getCanNextPage()}
                >
                  <MdKeyboardArrowRight />
                </button>
              </div>
            </div>
          </div>
        </div>
      </DndProvider>
    </div>
  );
};

export default TableTS;
